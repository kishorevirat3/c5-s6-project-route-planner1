package com.RoutePlanner;
import java.util.*;
import java.io.*;
import java.util.*;

public class RoutePlanner{
	public void showAllConnections(List<RouteDetails>list,String from,String to)
	{
		for(RouteDetails rd:list)
		{
			if(rd.getFrom().equals(from) && rd.getTo().equals(to))
			{
				System.out.printf("%13s %15s %18s %13s %25s",rd.getFrom(),rd.getTo(),rd.getDistance(),rd.getTravel_Time(),rd.getAirfare());
				System.out.println();
			}
		}
		for(RouteDetails rd:list)
		{
			if(rd.getFrom().equals(from))
			{
				for(RouteDetails rd1:list)
				{
					if(rd.getTo().equals(rd1.getFrom()) && rd1.getTo().equals(to))
					{
						System.out.printf("%13s %15s %18s %13s %25s",rd.getFrom(),rd.getTo(),rd.getDistance(),rd.getTravel_Time(),rd.getAirfare());
						System.out.println();
						System.out.printf("%13s %15s %18s %13s %25s",rd1.getFrom(),rd1.getTo(),rd1.getDistance(),rd1.getTravel_Time(),rd1.getAirfare());
						System.out.println();
					}
				}
			}
		}
	}
	public void sortDirectFlights(List<RouteDetails>list)
	{
		Collections.sort(list,new Comparator<RouteDetails>() {
			public int compare(RouteDetails r1, RouteDetails r2) {
				return r1.getTo().compareTo(r2.getTo());
			}
		});
		for(RouteDetails rd:list)
		{
			System.out.printf("%13s %15s %18s %13s %25s",rd.getFrom(),rd.getTo(),rd.getDistance(),rd.getTravel_Time(),rd.getAirfare());
			System.out.println();
		}
	}
	public List<RouteDetails> showDirectFlights(List<RouteDetails> list,String fromCity) {
		int cnt=0;
		List<RouteDetails>list1=new ArrayList<>();
		for(RouteDetails rd:list)
		{
			if(rd.getFrom().equals(fromCity))
			{
				list1.add(rd);
				System.out.printf("%13s %15s %18s %13s %25s",rd.getFrom(),rd.getTo(),rd.getDistance(),rd.getTravel_Time(),rd.getAirfare());
				System.out.println();
				cnt++;
			}
		}
		if(cnt==0)
		{
			System.out.println("There are no direct flights");
		}	
		return list1;
	}
	public List<RouteDetails> readFile(String filepath,List<RouteDetails>list)
	{
		try {
			BufferedReader br1=new BufferedReader(new FileReader(filepath));
			String line;
			br1.readLine();
			while((line=br1.readLine())!=null)
			{
			String[] values=line.split(",");
			String From=values[0];
			String To=values[1];
			String Distance=values[2];
			String Travel_Time=values[3];
			String AirFare=values[4];
			RouteDetails rp1=new RouteDetails(From, To, Distance, Travel_Time, AirFare);
			list.add(rp1);
			}
			br1.close();
		}
		catch(Exception e) {
	}
		return list;
	}
	public static void main(String[] args) {
		List<RouteDetails>list=new ArrayList<>();
		List<RouteDetails>list1=new ArrayList<>();
		Scanner sc=new Scanner(System.in);
		String Filepath="input.txt";
		RoutePlanner rp= new RoutePlanner();
		list=rp.readFile(Filepath,list);
		System.out.println("Route Details");
		System.out.printf("%12s %14s %20s %19s %18s","From", "To", "Distance", "Travel Time", "Airfare"); 
		System.out.println();
		System.out.println("---------------------------------------------------------------------------------------------------");
		for(RouteDetails rd:list)
		{
			System.out.printf("%13s %15s %18s %13s %25s",rd.getFrom(),rd.getTo(),rd.getDistance(),rd.getTravel_Time(),rd.getAirfare());
			System.out.println();
		}
		System.out.println("Enter From City: ");
		String fromcity1=sc.nextLine();
		String fromcity=fromcity1.substring(0,1).toUpperCase()+fromcity1.substring(1);
		System.out.println();
		System.out.println("Direct Flights From "+fromcity+" are: ");
		System.out.printf("%12s %14s %20s %19s %18s","From", "To", "Distance", "Travel Time", "Airfare"); 
		System.out.println();
		System.out.println("---------------------------------------------------------------------------------------------------");
		list1 =rp.showDirectFlights(list, fromcity);
		if(list1.size()>0)
		{
			System.out.println();
			System.out.println("Sorted Direct Flight Routes From "+fromcity+" are: ");
			System.out.printf("%12s %14s %20s %19s %18s","From", "To", "Distance", "Travel Time", "Airfare"); 
			System.out.println();
			System.out.println("---------------------------------------------------------------------------------------------------");
			rp.sortDirectFlights(list1);
		}
		else {
			System.out.println("Nothing to Sort");
		}
		System.out.println("Enter from city for Intermediate Flights: ");
		String from1=sc.nextLine();
		String from=from1.substring(0,1).toUpperCase()+from1.substring(1);
		System.out.println("Enter To city for Intermediate flights");
		String to1=sc.nextLine();
		String to=to1.substring(0,1).toUpperCase()+to1.substring(1);
		System.out.println();
		System.out.println("Intermediate Flight Details: ");
		System.out.printf("%12s %14s %20s %19s %18s","From", "To", "Distance", "Travel Time", "Airfare"); 
		System.out.println();
		System.out.println("---------------------------------------------------------------------------------------------------");
		rp.showAllConnections(list, from, to);
	}

}

